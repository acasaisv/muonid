import numpy as np

from copy import copy

absorb = [[12800, 28],  # ECAL
          [14300, 53],  # HCAL
          [15800, 47.5],  # M2
          [17100, 47.5],  # M3
          [18300, 47.5]]  # M4


def covij(i, j, p, z_m):
    if i > j:
        i, j = j, i
    val = 0
    higher = 5
    for l in range(higher):
        if (absorb[l][0] < z_m[i]) and (absorb[l][0] < z_m[j]):
            val += (z_m[i]-absorb[l][0]) * \
                (z_m[j]-absorb[l][0])*13.6**2*absorb[l][1]
    return val/p**2


def chi2_corr(padx, dx, mask, p):
    chi2corr_a = []
    chi2uncorr_a = []
    no_hits = []
    if p < 6000:
        mask[2]=mask[3]=0
    #####
    s = np.sum(mask)
    cov = np.zeros((s, s))
    z_m = np.array([15270, 16470, 17670, 18870])
    z_m = z_m[mask]

    for i_station in range(s):
        for j_station in range(i_station+1):
            print(i_station, j_station)
            cov[i_station, j_station] = covij(i_station, j_station, p, z_m)
            cov[j_station, i_station] = covij(i_station, j_station, p, z_m)
    covx = cov

    covxuncorr = np.zeros((s, s))
    for i in range(s):
        covx[i, i] += padx[i]**2
        covxuncorr[i, i] += padx[i]**2
    # print(covx)

    resx = np.all(np.linalg.eigvals(covx) > 0)

    if not (resx):
        print(
            f'Dimension {s}. Matrix x is {resx} PD. Matrix y is {resy} PD')
    chi2uncorr = (dx @ np.linalg.inv(covxuncorr) @ dx)/s
    chi2corr = (dx @ np.linalg.inv(covx) @ dx)/s

    return chi2corr, chi2uncorr


if __name__ == '__main__':

    padx = np.array([3.969, 17.032, 36.373])
    dx = np.array([127.630, 214.299, 361.993])

    mask = [0, 1, 1, 1]
    p = 6410.990
    corr, uncorr = chi2_corr(padx, dx, mask, p)
    print(f"Chi2Corr = {corr:.4f}, D2 = {uncorr:.4f} ")
