import numpy as np
import sys
import os
from multiprocessing.pool import ThreadPool
from subprocess import call 

def run(cmd):
    return cmd, call(cmd)
    
def actual_run(runfun,cmds,limit):
    for cmd, rc in ThreadPool(limit).imap_unordered(runfun,cmds):
        if rc != 0:
            print('{cmd} failed with exit status: {rc}'.format(**vars()))



def run_file(file,root='/scratch/acasaisv/newstack',outfile='monitoring.root'):
    os.chdir(root)
    command = f'./MooreOnline/build.x86_64_v2-el9-gcc12-opt/run python Allen/Dumpers/BinaryDumpers/options/allen.py --sequence=Allen/InstallArea/x86_64_v2-el9-gcc12-opt/constants/hlt1_pp_matching_then_forward_no_gec_no_ut.json --mep={file} --tags="master,master" --real-data --monitoring-file {outfile} -n -1'
    return command.split(' ')



if __name__ == '__main__':
    home = '/scratch/acasaisv/Allen-master/build'
    dir_finished_mep = '/scratch/acasaisv/run_parallel/output'
    dir_mep = '/calib/online/MEP_dumps_11_07_23/'
    meps = os.listdir(dir_mep)
    finished_meps = os.listdir(dir_finished_mep)
    #meps = list(filter(lambda x: '268259' in x,meps))
    out_meps = list(map(lambda x: dir_mep + x,meps))
    finished_meps = list(map(lambda x: dir_finished_mep + x,finished_meps))
    finished_meps = list(map(lambda x: x.replace('.root',''),finished_meps))
    remaining_meps = list(set(out_meps) - set(finished_meps))
    print(len(out_meps),len(finished_meps))
    commands = []
    for mep in remaining_meps:
        #print(mep)
        myrun = mep.split('/')[4]
        #print(run)
        commands .append(run_file(mep,outfile=f'/scratch/acasaisv/run_parallel/output/{myrun}.root'))
    actual_run(run,commands,limit=5)
    # home = '/home/submit/acasaisv/'
    # decay = 'b2kmumu'
    # pfns=open_pfns(home + f'pfns/{decay}.txt')
    # npf = 50
    # n_chunks = int(np.ceil(len(pfns)/npf))
    
    # commands = []
    # for i in range(n_chunks):
    #     myroot = f'/data/submit/lhcb/ntuples/data/2016/Stripping28r2/{decay}'
    #     if not(os.path.exists(myroot) and os.path.isdir(myroot)):
    #         os.makedirs(myroot)
    #     f_o = myroot + f'/{decay}_{i}.root'
    #     chunk = get_nth_chunk(pfns,i,npf)
    #     com = hadd_chunk(chunk,f_o)
    #     print(com)
    #     commands.append(com)
    
    # actual_run(run,commands,limit=10)
    

    

